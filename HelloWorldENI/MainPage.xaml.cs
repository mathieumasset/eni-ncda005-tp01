﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace HelloWorldENI
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        void Button_Clicked(System.Object sender, System.EventArgs e)
        {
            stackLayoutForms.IsVisible = true;
            stackLayoutTwitter.IsVisible = false;
            labelErrors.Text = null;

            if(String.IsNullOrEmpty(entryLogin.Text) || entryLogin.Text.Length <= 3)
            {
                labelErrors.Text = "Veuillez entrer un nom d'utilisateur valide";
                return;
            }

            if (String.IsNullOrEmpty(entryPassword.Text) || entryPassword.Text.Length <= 3)
            {
                labelErrors.Text = "Veuillez entrer un mot de passe valide";
                return;
            }

            stackLayoutForms.IsVisible = false;
            stackLayoutTwitter.IsVisible = true;
        }
    }
}
